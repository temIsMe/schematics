import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { AuthService } from './core/auth/auth.service';
import { SideMenuItem } from './layout/side-menu/side-menu-item';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dir: String;
  items: SideMenuItem[] = [
    { title: 'Website.Home.Title', iconClass: 'ti-home', url: 'home' },
    { title: 'Website.About.Title', iconClass: 'ti-arrow-top-right', url: 'about' },
    { title: 'Website.ContactUs.Title', iconClass: 'ti-shopping-cart', url: 'contact-us' }
  ];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );


  constructor(public auth: AuthService,
    private breakpointObserver: BreakpointObserver,
    private transService: TranslateService) {
    // sideMenuService.show = true;
    this.dir = transService.currentLang === 'he' ? 'rtl' : 'ltr';
  }

  title = 'app';
}
