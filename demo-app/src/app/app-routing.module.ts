import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { CallbackComponent } from './core/callback/callback.component';
import { AuthGuard } from './core/auth/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'callback', component: CallbackComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [
    AuthGuard
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
