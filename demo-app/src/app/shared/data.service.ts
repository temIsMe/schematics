import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PageMenuItem } from '../layout/page-menu/page-menu-item';

@Injectable()
export class DataService {

  private pageMenuSource = new BehaviorSubject<PageMenuItem[]>([]);
  currentPageMenu = this.pageMenuSource.asObservable();

  constructor() { }

  changePageMenu(pageMenu: PageMenuItem[]) {
    this.pageMenuSource.next(pageMenu);
  }

}
