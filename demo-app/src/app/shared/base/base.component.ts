import { DataService } from './../data.service';
import { OnInit, OnDestroy } from '@angular/core';
import { AppInjector } from 'src/app/app.module';
import { PageMenuItem } from 'src/app/layout/page-menu/page-menu-item';


export class BaseComponent implements OnInit, OnDestroy {
    dataService: DataService;

    constructor(menuItems: PageMenuItem[]) {
        this.dataService = AppInjector.get(DataService);
        this.dataService.changePageMenu(menuItems);
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        this.dataService.changePageMenu([]);
    }
}
