import { AfterViewInit, ElementRef, ViewChild, OnInit, Injectable } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/internal/operators';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { PageMenuItem } from 'src/app/layout/page-menu/page-menu-item';


@Injectable()
export class BaseListComponent<T> extends BaseComponent implements AfterViewInit, OnInit {

  pageSize = 3;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(public dataSource, menuItems: PageMenuItem[]) {
    super(menuItems);
  }

  ngOnInit() {
    this.loadContactsPage();
    super.ngOnInit();
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;

          this.loadContactsPage();
        })
      )
      .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadContactsPage())
      )
      .subscribe();

  }

  loadContactsPage() {
    // console.log(this.sort);
    this.dataSource.loadItems(
      this.input.nativeElement.value,
      this.sort.direction || 'asc',
      this.sort.active,
      this.paginator.pageIndex || 0,
      this.paginator.pageSize || this.pageSize);
  }


  deleteMe(item: T) {
    this.dataSource.removeItem(item);
  }

}
