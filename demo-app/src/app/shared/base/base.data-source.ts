import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/internal/operators';



/**
 * Data source for the Contact view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class BaseDataSource<T> extends DataSource<T> {


  private itemsSubject = new BehaviorSubject<T[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public itemsCount = new BehaviorSubject<number>(0);
  public loading$ = this.loadingSubject.asObservable();

  constructor(public service) {
    super();
  }


  loadItems(filter: string = '',  sortDirection, sortBy: string = '_id', pageIndex, pageSize) {

    this.loadingSubject.next(true);

    const params = { filter, sortDirection, sortBy, pageNumber: pageIndex.toString(), pageSize: pageSize.toString() };

    this.service.getIndex(params)
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(res => {
        this.itemsSubject.next(res['docs']);
        this.itemsCount.next(res['total']);
      });

  }

  removeItem(model: T) {
    this.service.delete(model['_id']).subscribe(() => {
      const items: any[] = this.itemsSubject.getValue();
      items.forEach((item, index) => {
        if (item['_id'] === item['_id']) {
          items.splice(index, 1);
        }
      });
      this.itemsSubject.next(items);
    });
  }

  connect(collectionViewer: CollectionViewer): Observable<T[]> {
    // console.log("Connecting data source");
    return this.itemsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.itemsSubject.complete();
    this.loadingSubject.complete();
  }

}
