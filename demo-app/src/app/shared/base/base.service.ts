import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import {catchError, map} from 'rxjs/internal/operators';

export class BaseService {

  api_url;
  http;
  requestConfig;

  constructor(plural: string, sentHttp: HttpClient) {
    this.http = sentHttp;
    this.api_url = environment.apiUrl + '/' + plural;
    this.setBearer();
  }

  /** send token to server, */
  setBearer() {
    this.requestConfig = {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${localStorage.getItem('access_token')}`
      )
    };
  }

  getList(): Observable<any[]> {
    return this.http
      .get(this.api_url, this.requestConfig)
      .pipe(
        map(response =>  response as any[]),
        catchError(this.handleError)
      );
  }

  getIndex(params): Observable<any[]> {

    return this.http
      .get(this.api_url, {
        params: params,
        headers: this.requestConfig.headers
      })
      .pipe(
        map(response =>  response as any[]),
        catchError(this.handleError)
      );
  }

  getSingle(id: string): Observable<any> {
    return this.http
      .get(this.api_url + '/' + id, this.requestConfig)
      .pipe(
        map(response =>  response as any),
        catchError(this.handleError)
      );
  }


  create(model: any): Observable<any> {
   // model._id = Math.random().toString(36).substr(2);
    return this.http
      .post(this.api_url, model, this.requestConfig)
      .pipe(
        map(response =>  response as any),
        catchError(this.handleError)
      );
  }


  update(model: any): Observable<any> {
    return this.http
      .put(this.api_url + '/' + model._id, model, this.requestConfig)
      .pipe(
        map(response => {
          return model.__v = response['__v'];
        }),
        catchError(this.handleError)
      );
  }

  delete(id: any): Observable<null> {
    return this.http
      .delete(this.api_url + '/' + id, this.requestConfig)
      .pipe(
        map(response => null),
        catchError(this.handleError)
      );
  }


  handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return of(error);
  }

}
