// angular
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// for AoT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateLoader {
  return new TranslateHttpLoader(http, './assets/i18n/');
}

@NgModule({
  imports: [
    TranslateModule.forRoot()
  ],
  declarations: [
    // ChangeLanguageComponent
  ],
  providers: [
    // I18NService
  ],
  exports: [
    TranslateModule
  ]
})
export class I18NModule {
  static forRoot(configuredProviders?: Array<any>): ModuleWithProviders {
    return {
      ngModule: I18NModule,
      providers: configuredProviders
    };
  }

  constructor(translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');

    const lang = 'he';
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use(lang);

  }
}
