// import {environment} from '../../environments/environment';

interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_DOMAIN: string;
  AUDIENCE: string;
  REDIRECT: string;
  SCOPE: string;
}


export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: '801b1Ap1fyj2sAXsEh4rbijUUeUyqyA7',
  CLIENT_DOMAIN: 'dcdr.eu.auth0.com', // e.g., you.auth0.com
  AUDIENCE: location.origin + '/api/',
  REDIRECT: location.origin + '/callback',
  SCOPE: 'openid profile email'
};
