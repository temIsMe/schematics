import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { Router } from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html'
})
export class CallbackComponent implements OnInit, OnDestroy {
  loggedInSub: Subscription;

  /** passport login success, sends token to callback*/
  constructor(private auth: AuthService, private router: Router) {
    // Parse authentication hash
    auth.handleAuth();
  }

  ngOnInit() {
    this.loggedInSub = this.auth.loggedIn$.subscribe(
      loggedIn => loggedIn ? this.router.navigate(['/contacts']) : null
    );
  }

  ngOnDestroy() {
    this.loggedInSub.unsubscribe();
  }

}
