import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared/base/base.service';


@Injectable()
export class  UserService extends BaseService {

    constructor(http: HttpClient) {
        super('users', http);
    }

}
