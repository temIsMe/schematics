import { WindowModule } from '@ng-toolkit/universal';
// import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { SideMenuComponent } from './layout/side-menu/side-menu.component';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { ListService } from './shared/list.service';
import { AuthService } from './core/auth/auth.service';
import { UserService } from './core/user/user.service';
import { CallbackComponent } from './core/callback/callback.component';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule
} from '@angular/material';
import { SharedModule } from './shared/shared.module';
import { PageMenuComponent } from './layout/page-menu/page-menu.component';
import { I18NModule, HttpLoaderFactory } from './core/i18n/i18n.module';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { NavigationService } from './layout/navigation.service';
import { DashboardModule } from './+dashboard/dashboard.module';
import { WebSiteModule } from './+web-site/web-site.module';

export let AppInjector: Injector;

@NgModule({
  imports: [
    CommonModule,
    WindowModule,
    I18NModule.forRoot([
      {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    ]),
    DashboardModule,
    AppRoutingModule,
    WebSiteModule,
    LayoutModule,
    SharedModule,
    MatToolbarModule,
    MatSidenavModule,
    MatMenuModule
  ],
  declarations: [
    AppComponent,
    SideMenuComponent,
    CallbackComponent,
    HeaderComponent,
    PageMenuComponent
  ],

  providers: [ListService, AuthService, UserService, NavigationService],
  exports: [SideMenuComponent, HeaderComponent, PageMenuComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    AppInjector = this.injector;
  }
}
