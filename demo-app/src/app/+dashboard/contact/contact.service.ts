import { PageMenuItem } from './../../layout/page-menu/page-menu-item';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../shared/base/base.service';


@Injectable()
export class ContactService extends BaseService {

  listMenu: PageMenuItem[] = [
    {
      title: 'Redial', icon: 'dialpad', action: function () {
        console.log('sdf');
      }
    },
    { title: 'Check voicemail', icon: 'voicemail' },
    { title: 'Disable alerts', icon: 'notifications_off' }
  ];

  constructor(http: HttpClient) {
    super('contacts', http);
  }


  getListMenu(): PageMenuItem[] {
    return this.listMenu;
  }
}
