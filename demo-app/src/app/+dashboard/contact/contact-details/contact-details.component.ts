import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Contact} from '../contact.model';
import {ContactService} from '../contact.service';

@Component({
    selector: 'app-contact-details',
    templateUrl: './contact-details.component.html',
    styleUrls: ['./contact-details.component.scss']
})
export class  ContactDetailsComponent implements OnInit {
    contact:  Contact;
    errors: string;

    constructor(private service:  ContactService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
      this.contact = this.route.snapshot.data['entity'] || new Contact();
    }

    saveMe() {
      this.service[this.contact._id ? 'update' : 'create'](this.contact).subscribe(() => {
        this.router.navigate(['contacts']);
      });
    }
}
