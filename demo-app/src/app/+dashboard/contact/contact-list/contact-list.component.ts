
import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact.model';
import { ContactService } from '../contact.service';
import { ContactDataSource } from './contact-list.data-source';
import { BaseListComponent } from 'src/app/shared/base/base-list.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent extends BaseListComponent<Contact> {

  displayedColumns = ['_id', 'name', 'city', 'stars', 'delete'];

  constructor( service: ContactService) {
    super(new ContactDataSource(service), service.getListMenu());
  }

}
