import { Contact } from '../contact.model';
import { ContactService } from '../contact.service';
import { BaseDataSource } from '../../../shared/base/base.data-source';

export class ContactDataSource extends BaseDataSource<Contact> {

  constructor(service: ContactService) {
    super(service);
  }

}
