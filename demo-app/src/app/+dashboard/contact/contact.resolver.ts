import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ContactService} from './contact.service';
import {Contact} from './contact.model';

@Injectable()
export class ContactResolver implements Resolve<Contact> {

  constructor(private service: ContactService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return route.params['id'] !== 'add' ? this.service.getSingle(route.params['id']) : null;
  }

}
