import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactResolver } from './contact.resolver';
import { AuthGuard } from 'src/app/core/auth/auth.guard';

export const ContactRoute = {
  path: 'contacts',
  children: [
    {
      path: '',
      component: ContactListComponent,
      canActivate: [ AuthGuard]
    },
    {
      path: ':id',
      component: ContactDetailsComponent,
      data: {
          menuBack: true
      },
      canActivate: [ AuthGuard],
      resolve: {
        entity: ContactResolver
      }
    }
  ]
};

