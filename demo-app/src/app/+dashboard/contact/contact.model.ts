export class Contact {
  _id: string;
  name: string;
  city?: string;
  stars?: string;
}
