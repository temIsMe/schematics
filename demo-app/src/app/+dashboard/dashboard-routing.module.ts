import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactRoute} from './contact/contact.routes';
import {CLIENT_ROUTES} from './client/client.routes';


const routes: Routes = [
  ContactRoute,
  CLIENT_ROUTES
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
