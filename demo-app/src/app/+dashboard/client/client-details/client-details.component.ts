import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Client} from '../client.model';
import { ClientService} from '../client.service';

@Component({
    selector: 'app-client-details',
    templateUrl: './client-details.component.html',
    styleUrls: ['./client-details.component.scss']
})
export class  ClientDetailsComponent implements OnInit {
    client:  Client;
    errors: string;

    constructor(private service:  ClientService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.client = this.route.snapshot.data['entity'] || new Client();
    }

    saveMe() {
        this.service[this.client._id ? 'update' : 'create'](this.client).subscribe(() => {
            this.router.navigate(['clients']);
        });
    }

}
