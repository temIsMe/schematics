import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared/base/base.service';


@Injectable()
export class  ClientService extends BaseService {

    constructor(http: HttpClient) {
        super('clients', http);
    }

}
