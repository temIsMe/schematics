import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ClientService} from './client.service';
import {Client} from './client.model';

@Injectable()
export class ClientResolver  implements Resolve<Client> {

  constructor(private service: ClientService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return route.params['id'] != 'add' ? this.service.getSingle(route.params['id']) : null;
  }

}
