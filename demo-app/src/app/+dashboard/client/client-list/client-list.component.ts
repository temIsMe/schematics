import {Component, OnInit} from '@angular/core';
import {Client} from '../client.model';
import {ClientService} from '../client.service';
import {ListService} from '../../../shared/list.service';

@Component({
    selector: 'app-client-list',
    templateUrl: './client-list.component.html',
    styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

    clients: Client[];

    constructor(private service: ClientService,
        private listService: ListService) {
    }

    ngOnInit() {
        this.service.getList()
            .subscribe(items => this.clients = items);
    }

    delete(client: Client) {
        this.service.delete(client._id).subscribe(() => {
            this.listService.removeItem(this.clients, client._id);
        });
    }
}
