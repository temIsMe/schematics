import { ClientListComponent} from './client-list/client-list.component';
import { ClientDetailsComponent} from './client-details/client-details.component';
import { ClientResolver} from './client.resolver';

export const CLIENT_ROUTES =
    {
        path: 'clients',
        children: [
            {
                path: '',
                component:  ClientListComponent
            },
            {
                path: ':id',
                component:  ClientDetailsComponent,
                resolve: {
                    entity:  ClientResolver
                }
            }
        ]
    }
;
