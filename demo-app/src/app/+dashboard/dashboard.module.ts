import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';


import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {ContactListComponent} from './contact/contact-list/contact-list.component';
import {ContactDetailsComponent} from './contact/contact-details/contact-details.component';
import {ContactResolver} from './contact/contact.resolver';
import {ContactService} from './contact/contact.service';

import { ClientDetailsComponent } from './client/client-details/client-details.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { ClientService } from './client/client.service';
import { ClientResolver } from './client/client.resolver';
import {MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule} from '@angular/material';
import {SharedModule} from '../shared/shared.module';




@NgModule({
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    DashboardRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule
  ],
  declarations: [ ContactListComponent, ContactDetailsComponent, ClientDetailsComponent, ClientListComponent],
  providers: [ContactService, ContactResolver,  ClientService, ClientResolver]
})
export class DashboardModule { }
