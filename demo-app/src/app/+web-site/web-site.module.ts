import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import {WebSiteRoutingModule} from './web-site-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WebSiteRoutingModule
  ],
  declarations: [HomeComponent, AboutComponent, ContactUsComponent]
})
export class WebSiteModule { }
