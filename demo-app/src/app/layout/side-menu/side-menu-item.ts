export interface SideMenuItem {
    title: string;
    url?: string;
    iconClass?: string;
}
