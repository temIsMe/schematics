import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Data } from '@angular/router';

@Injectable()
export class NavigationService {
  private iconSubject = new BehaviorSubject<string>('Page Title');
  currentIcon = this.iconSubject.asObservable();


  constructor() {
  }


  routeChanged(data: Data): any {
    if (data.title) {

    }

    const icon = data.menuBack ? 'arrow_back_ios' : 'menu';
    this.iconSubject.next(icon);
   
  }

}
