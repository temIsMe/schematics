import { Component, Input, OnInit } from '@angular/core';

import { MatSidenav } from '@angular/material/sidenav';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { mergeMap, filter, map } from 'rxjs/operators';
import { NavigationService } from '../navigation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input('drawer') drawer: MatSidenav;
  headerIcon = 'menu'; // arrow_back_ios

  constructor(private router: Router,
    private navigationService: NavigationService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.navigationService.currentIcon.subscribe(icon => this.headerIcon = icon);

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    ).subscribe((event) => {
      this.navigationService.routeChanged(event);
    });
  }

  menuClicked() {
    this.drawer.toggle();  
  }

}
