export class PageMenuItem {
    title: string;
    icon?: string;
    action?: () => void;

    constructor() {
    }
}
