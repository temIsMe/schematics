import { Component, OnInit } from '@angular/core';
import { PageMenuItem } from './page-menu-item';
import { DataService } from '../../shared/data.service';

@Component({
  selector: 'app-page-menu',
  templateUrl: './page-menu.component.html',
  styleUrls: ['./page-menu.component.scss']
})
export class PageMenuComponent implements OnInit {

  items: PageMenuItem[];

  constructor(private dataService: DataService) {  
  }

  ngOnInit() {
    this.dataService.currentPageMenu.subscribe(items => this.items = items);
  }

}
