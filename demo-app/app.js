var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');



// var pass=encodeURIComponent('dcdr#123');
function getConnection(){
  return process.env.CUSTOMCONNSTR_atlas.replace('{0}',  encodeURIComponent(process.env.atlas_pass));
}

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var db =
  process.env.CUSTOMCONNSTR_atlas ?
    getConnection():
    //'mongodb://Bigchiff:'+pass+'@clusterdecieder-shard-00-00-x0vsa.mongodb.net:27017,clusterdecieder-shard-00-01-x0vsa.mongodb.net:27017,clusterdecieder-shard-00-02-x0vsa.mongodb.net:27017/decider?ssl=true&replicaSet=ClusterDecieder-shard-0&authSource=admin' ||
    'mongodb://127.0.0.1:27017/myDb';

mongoose.connect(db, { promiseLibrary: require('bluebird') })
  .then(() => {
  console.log(`Succesfully Connected to the Mongodb Database  at URL : ` + db)
})
.catch((e) => {
  console.log(`Error Connecting to  : ` + db, e)
});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});


app.use(favicon(path.join(__dirname, 'dist/browser', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());


var api = require('./server/routes/api.route');
app.use('/api', api);

const DIST_FOLDER = 'dist/browser';
/** User Angular files from dist folder*/
app.use(express.static(path.join(__dirname, DIST_FOLDER)));
app.use('*', express.static(path.join(__dirname, DIST_FOLDER)));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(req);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message ;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log("ERROR", res.message );
  res.render('error');
});

module.exports = app;
