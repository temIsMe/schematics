var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EntitySchema = new mongoose.Schema({
  name: String,
  email: String,
  picture: String,
  updated_at: Date,
  sub: String,
  accessToken: String

});


module.exports = mongoose.model('User', EntitySchema);
