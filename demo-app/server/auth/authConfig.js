
module.exports =  {
  CLIENT_DOMAIN: 'dcdr.eu.auth0.com', // e.g. 'you.auth0.com'
  AUTH0_AUDIENCE: (process.env.url || 'http://localhost:3000')  + /api/
};
