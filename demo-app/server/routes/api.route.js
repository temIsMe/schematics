var router = require('express').Router();

router.use('/contacts', new (require("../controllers/contactController"))().routes());
router.use('/users', new (require("../controllers/userController"))().routes());

module.exports = router;
