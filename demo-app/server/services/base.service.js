// Async function to get the To do List
exports.getMany = async function (model) {
  try {
    return await model.find({});
  } catch (e) {
    throw e;
  }
};

exports.getSingle = async function (model, id) {
  try {
    return await model.findById(id);
  } catch (e) {
    throw e;
  }
};

exports.create = async function (model, data) {
  try {
    return await model.create(data);
  } catch (e) {
    throw e;
  }
};

exports.update = async function (model, id, data) {
  try {
    return await model.findById(id)
      .then((modelInstance) => {
        for (var attribute in data) {
          if (data.hasOwnProperty(attribute) && attribute !== this.key && attribute !== "_id") {
            modelInstance[attribute] = data[attribute];
          }
        }

        return modelInstance.save();
      });
  } catch (e) {
    throw e;
  }

};

exports.delete = async function (model, id) {
  try {
    return await model
      .remove({_id: id});
  } catch (e) {
    throw e;
  }
};
