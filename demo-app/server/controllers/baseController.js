var EntityService = require('../services/base.service');
var jwtCheck = require('../auth/auth');
const jwt = require('jsonwebtoken');
var User = require('../models/user.model');

class BaseController {


  /**
   @param model Mongoose model
   @param key primary key of the model that will be used for searching, removing
   and reading
   */
  constructor(model, routerSrc, key = "_id") {
    this.router = routerSrc;
    this.model = model;
    this.modelName = model.modelName.toLowerCase();
    this.logins = [];
    this.key = key;
    this.service = EntityService;
  }

  async extractUser(req) {


    var token = req.headers['authorization'].split(' ')[1];
    var dtoken = jwt.decode(token, {complete: true}) || {};

    var user = this.logins.find(x => x.sub === dtoken.payload.sub);

    if (user != null) {
     // console.log("info cache", user);
      return   this.userId = user._id;
    }

    return await User.findOne({sub: dtoken.payload.sub}, 'sub')
      .then((user) => {
        if (user && user._id) {
          this.logins.push(user);
          //console.log("info server", user);
          return   this.userId = user._id;
        }

        console.log("error extractUser", user,dtoken.payload);
      });
  }

  result(data) {
    return data;
  }

  async drop() {
    return this.list();
  }

  async list(queryParams) {
    if(queryParams && queryParams.pageNumber)
      return this.paginate(queryParams);

    return await EntityService.getMany(this.model)
      .then(this.result);
  }

  async paginate(queryParams) {
    
    const query = queryParams.filter ? {
      $text: { $search: queryParams.filter }
    } : {},
      sortDirection = queryParams.sortDirection == 'asc' ? '' : '-',
      sortBy= sortDirection + (queryParams.sortBy || '_id'),
      pageNumber = (parseInt(queryParams.pageNumber) || 0) + 1,
      pageSize = parseInt(queryParams.pageSize) ;
  
    return await this.model.paginate(query, 
      { 
        page: pageNumber, 
        limit: pageSize, 
        sort: sortBy
      });

  }

  async read(id) {
    return await EntityService.getSingle(this.model, id)
      .then(this.result);
  }

  async create(data) {
    return await EntityService.create(this.model, data)
      .then(this.result);
  }

  async update(id, data) {
    return await EntityService.update(this.model, id, data)
      .then((data) => {
        return {__v: data.__v};
      });
  }

  async delete(id) {
    return await EntityService.delete(this.model, id)
      .then(() => {
        return {};
      });
  }

  routes() {
    var router = this.router;

    /**
     Returns a function that will write the result as a JSON to the response
     */
    function ok(res) {
      return (data) => {
        res.json(data);
      };
    }

    /**
     Depending on the error type, will perform the following:
     Object was not found - 404 Not Found
     Invalid or missing input parameter - 400 Bad request
     Not enough privileges - 401 Unauthorized
     Unknown error - 500 Internal server error
     */
    function fail(res) {
      return (error) => {
        console.log("******   BaseController Error " + error._message, error);
        res.sendStatus(404).end();
      };
    }


    router.get("/", jwtCheck, (req, res) => {

      this.extractUser(req).then(() => {

       // console.log("userId " + userId);
        this[req.query.func || 'list'](req.query)
          .then(ok(res))
          .then(null, fail(res));
      });

    });

    router.get("/:key", jwtCheck, (req, res) => {
      this.extractUser(req).then(() => {
        this
          .read(req.params.key)
          .then(ok(res))
          .then(null, fail(res));
      });

    });

    router.post("/", jwtCheck, (req, res) => {
      this.extractUser(req).then(() => {
        req.body.user = this.userId;
        this.create(req.body, req)
          .then(ok(res))
          .then(null, fail(res));
      });

    });



    router.put("/:key", jwtCheck, (req, res) => {
      this.extractUser(req).then(() => {
        this
          .update(req.params.key, req.body)
          .then(ok(res))
          .then(null, fail(res));
      });

    });

    router.delete("/:key/:childKey", jwtCheck, (req, res) => {
      this.extractUser(req).then(() => {
        this
          .delete(req.params.key, req.params.childKey)
          .then(ok(res))
          .then(null, fail(res));
      });

    });


    router.delete("/:key", jwtCheck, (req, res) => {
      this.extractUser(req).then(() => {
        this
          .delete(req.params.key)
          .then(ok(res))
          .then(null, fail(res));
      });

    });


    return router;
  }


}

module.exports = BaseController;
