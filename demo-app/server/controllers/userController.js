var express = require('express');
var router = express.Router();
var BaseController = require('./baseController');
var mongoModel = require('../models/user.model');

class UserController extends BaseController {

  constructor() {
    super(mongoModel,router);
  }


  async create(data, req) {
    function doneRes(res) {

      return {
        data: {
          _id: res._id
        }
      };
    }

    return await this.model.findOne({email: data.profile.email}, 'email')
      .then((user) => {

        if (user && user._id) {
          return this.change(user._id, data).then(doneRes);
        }
        return this.add(data).then(doneRes);
      });
  }


  async add(data) {
    return await this.service.create(this.model, data.profile);
  }

  async change(id, data) {
    return await this.service.update(this.model, id, data.profile);
  }
}

module.exports = UserController;
