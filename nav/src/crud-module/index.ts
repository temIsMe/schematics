import {
    apply,
    branchAndMerge,
    chain,
    mergeWith,
    move,
    Rule,
    SchematicContext,
    SchematicsException,
    template,
    Tree,
    url
} from '@angular-devkit/schematics';
import {CrudOptions} from './schema';
import {strings} from '@angular-devkit/core';
import {findModuleFromOptions} from '../schematics-angular-utils/find-module';
import * as crudModelUtils from '../utils/crud-model-utils';
import {CrudModel} from './model';
import * as JSON5 from 'json5';
import {parseName} from '../utils/parse-name';
import {getWorkspace} from '../schematics-angular-utils/config';
import {addDeclarationToNgModule} from '../utils/ng-module-utils';
import {injectServiceIntoAppComponent, injectSome} from '../utils/add-injection';


export default function (options: CrudOptions): Rule {

    function getModelJson(host: Tree, name: string) {
        const modelFile = `mock/models.json`;
        const modelBuffer = host.read(modelFile);

        if (modelBuffer === null) {
            throw new SchematicsException(`Models json file does not exist.`);
        }

        const modelsJson = JSON5.parse(modelBuffer.toString('utf-8'));

        if (!modelsJson[name]) {
            throw new SchematicsException(`Model file ${name} does not exist in models.json.`);
        }

        const model = modelsJson[name] as CrudModel;

        // console.log("modelFile : ", model);

        return model;
    }

    function createTemplate(model: CrudModel, path: string, folder: string) {
        const clientSource = apply(url(folder), [

            template({
                ...strings,
                ...options,
                ...crudModelUtils as any,
                model
            }),
            () => {
                console.debug('path: ', path)
            },

            move(path)
        ]);
        return clientSource;
    }

    return (host: Tree, context: SchematicContext) => {


        const workspace = getWorkspace(host);
        if (!options.project) {
            options.project = Object.keys(workspace.projects)[0];
        }
        const project = workspace.projects[options.project];

        if (options.path === undefined) {
            const projectDirName = project.projectType === 'application' ? 'app' : 'lib';
            options.path = `${project.root}/src/${projectDirName}`;
        }


        const folder = options.module ? `/${options.module}` : '';
        const path = `${options.path}${folder}/${options.name}`;

        options.module = findModuleFromOptions(host, options);

        const parsedPath = parseName(options.path, options.name);
        options.name = parsedPath.name;
        options.path = parsedPath.path;


        const model = getModelJson(host, options.name);
        const clientSource = createTemplate(model, path, './files');
        const serverSource = createTemplate(model, '/server', './serverFiles');

        const rule = chain([
            branchAndMerge(chain([

                 mergeWith(clientSource), //WORKING
                 mergeWith(serverSource), //WORKING
                 addDeclarationToNgModule(options, options.export) //WORKING
                // injectServiceIntoAppComponent(options)
               // injectSome(options)
            ]))
        ]);


        return rule(host, context);
    }
}