
export interface CrudOptions {
    name: string;
    path?: string;
    appRoot: string;
    sourceDir: string;
    module?: string;
    export: boolean;
    model: string;
    project?: string;
    service?: boolean;
}