import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { <%= classify(name) %>} from '../<%=dasherize(name)%>.model';
import { <%= classify(name) %>Service} from '../<%=dasherize(name)%>.service';

@Component({
    selector: 'app-<%=dasherize(name)%>-details',
    templateUrl: './<%=dasherize(name)%>-details.component.html',
    styleUrls: ['./<%=dasherize(name)%>-details.component.scss']
})
export class  <%= classify(name) %>DetailsComponent implements OnInit {
    <%=dasherize(name)%>:  <%= classify(name) %>;
    errors: string;

    constructor(private service:  <%= classify(name) %>Service,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.<%=dasherize(name)%> = this.route.snapshot.data['entity'] || new <%=classify(name)%>();
    }

    saveMe() {
        this.service[this.<%=dasherize(name)%>._id ? 'update' : 'create'](this.<%=dasherize(name)%>).subscribe((res) => {
            this.router.navigate(['<%=dasherize(name)%>s', this.<%=dasherize(name)%>._id || res._id]]);
        });
     
    }

}
