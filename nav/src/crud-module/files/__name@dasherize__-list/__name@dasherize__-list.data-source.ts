import {<%= classify(name) %>} from '../<%=dasherize(name)%>.model';
import {<%= classify(name) %>Service} from '../<%=dasherize(name)%>.service';
import { BaseDataSource } from '../../../shared/base/base.data-source';

export class <%=classify(name)%>DataSource extends BaseDataSource<<%=classify(name)%>> {

  constructor(service: <%=classify(name)%>Service) {
    super(service);
  }

}
