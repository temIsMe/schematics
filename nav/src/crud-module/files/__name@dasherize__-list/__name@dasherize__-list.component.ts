import {Component} from '@angular/core';
import {<%= classify(name) %>} from '../<%=dasherize(name)%>.model';
import {<%= classify(name) %>Service} from '../<%=dasherize(name)%>.service';
import { <%= classify(name) %>DataSource } from './<%=dasherize(name)%>-list.data-source';
import { BaseListComponent } from '../../../shared/base/base-list.component';


@Component({
    selector: 'app-<%=dasherize(name)%>-list',
    templateUrl: './<%=dasherize(name)%>-list.component.html',
    styleUrls: ['./<%=dasherize(name)%>-list.component.scss']
})
export class <%= classify(name) %>ListComponent extends BaseListComponent<<%= classify(name) %>> {

    displayedColumns = ['_id', <% for (let field of model.fields) { %> '<%=field.name%>', <% } %> 'delete'];
    
    constructor(service: <%= classify(name) %>Service) {
      super(new <%= classify(name) %>DataSource(service));
    }
}
