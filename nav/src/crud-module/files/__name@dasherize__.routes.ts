import { <%= classify(name) %>ListComponent} from './<%=dasherize(name)%>-list/<%=dasherize(name)%>-list.component';
import { <%= classify(name) %>DetailsComponent} from './<%=dasherize(name)%>-details/<%=dasherize(name)%>-details.component';
import { <%= classify(name) %>Resolver} from './<%=dasherize(name)%>.resolver';
import { AuthGuard } from 'src/app/core/auth/auth.guard';

export const <%=name.toUpperCase()%>_ROUTES = {
    path: '<%=(dasherize(name) + plural)%>',
    canActivate: [ AuthGuard],
    children: [
        {
            path: '',
            component:  <%= classify(name) %>ListComponent,
            data: {
                title: '<%=classify(name)%>.List.Title'
            },
        },
        {
            path: ':id',
            component:  <%= classify(name) %>DetailsComponent,
            data: {
                menuBack: '<%=(dasherize(name) + plural)%>',
                entityTitle: 'name'
            },
            resolve: {
                entity:  <%= classify(name) %>Resolver
            }
        }
    ]
};
