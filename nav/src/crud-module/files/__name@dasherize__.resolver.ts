import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {<%= classify(name) %>Service} from './<%=dasherize(name)%>.service';
import {<%= classify(name) %>} from './<%=dasherize(name)%>.model';

@Injectable()
export class <%= classify(name) %>Resolver  implements Resolve<<%=classify(name)%>> {

  constructor(private service: <%= classify(name) %>Service) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return route.params['id'] !== 'add' && route.routeConfig.path !== 'add' ? this.service.getSingle(route.params['id']) : null;
  }

}
