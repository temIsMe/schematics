import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { BaseService } from '../../shared/base/base.service';
import { PageMenuItem } from '../../layout/page-menu/page-menu-item';

@Injectable()
export class  <%= classify(name) %>Service extends BaseService {
    listMenu: PageMenuItem[] = [
        
      ];

    constructor(http: HttpClient) {
        super('<%=dasherize(name)%>s', http);
    }

    getListMenu(): PageMenuItem[] {
        return this.listMenu;
    }
}
