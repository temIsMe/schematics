export class <%= classify(name) %> {
    _id: string;
    isArchive: boolean;
    <% for (let field of model.fields) { %> <%=field.name%>:  <%=field.type.ref || field.type%>;
    <% } %>
}