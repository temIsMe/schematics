import {Rule, SchematicsException, Tree} from '@angular-devkit/schematics';
import {AddToModuleContext} from './add-to-module-context';
import * as ts from 'typescript';
import {strings} from '@angular-devkit/core';
import {buildRelativePath, ModuleOptions} from '../schematics-angular-utils/find-module';
import {addDeclarationToModule, addProviderToModule} from '../schematics-angular-utils/ast-utils';
import {InsertChange} from '../schematics-angular-utils/change';

const {dasherize, classify} = strings;

const stringUtils = {dasherize, classify};
const refs = [
    {func: addDeclarationToModule, type: 'Component', name: 'Details'},
    {func: addDeclarationToModule, type: 'Component', name: 'List'},
    // { func: addExportToModule, type: ''},
    {func: addProviderToModule, type: 'Service', name: ''},
    {func: addProviderToModule, type: 'Resolver', name: ''},
];



export function addDeclarationToNgModule(options: ModuleOptions, exports: boolean): Rule {
    return (host: Tree) => {

        refs.forEach(x => addRef(host, options, x));

        return host;
    };
}


function addRef(host: Tree, options: ModuleOptions, aRef: any) {

    const context = createAddToModuleContext(host, options, aRef);

    const modulePath = options.module || '';

    const changes = aRef.func(context.source,
        modulePath,
        context.classifiedName,
        context.relativePath);

    const recorder = host.beginUpdate(modulePath);
    for (const change of changes) {
        if (change instanceof InsertChange) {
            recorder.insertLeft(change.pos, change.toAdd);
        }
    }
    host.commitUpdate(recorder);
}


function pathByType(options: ModuleOptions, aRef: any) {
    const lowerName = stringUtils.dasherize(options.name);//user
    const folder =
        '/' +//`${options.path}/` + //src/app
        lowerName + '/'
    ;

    if (aRef.type == 'Component') {
        const dashed =  lowerName + '-' + aRef.name.toLowerCase(); //user-details
        return folder +
            dashed + '/' +  // user-details/
            dashed + '.'  + // user-details.
            aRef.type.toLowerCase(); // component
    }

    return folder
        + lowerName + '.' // user.
        + aRef.type.toLowerCase(); // service

    // return folder
    //     + lowerName
    //     + aRef.type;

}

function createAddToModuleContext(host: Tree, options: ModuleOptions, aRef: any): AddToModuleContext {

    const result = new AddToModuleContext();

    if (!options.module) {
        throw new SchematicsException(`Module not found.`);
    }

    const text = host.read(options.module);

    if (text === null) {
        throw new SchematicsException(`File ${options.module} does not exist!`);
    }
    const sourceText = text.toString('utf-8');
    result.source = ts.createSourceFile(options.module, sourceText, ts.ScriptTarget.Latest, true);

    const filePath = pathByType(options, aRef);

    result.relativePath =  '.' + filePath;//buildRelativePath(options.module, filePath);
     console.log( result.relativePath );

    result.classifiedName = stringUtils.classify(`${options.name}${aRef.name}${aRef.type}`);

    // console.log( result.relativePath, result.classifiedName);

    return result;

}

//
// function addDeclaration(host: Tree, options: ModuleOptions, type: string) {
//
//     const context = createAddToModuleContext(host, options,type);
//
//     const modulePath = options.module || '';
//
//     const declarationChanges = addDeclarationToModule(context.source,
//         modulePath,
//         context.classifiedName,
//         context.relativePath);
//
//     const declarationRecorder = host.beginUpdate(modulePath);
//     for (const change of declarationChanges) {
//         if (change instanceof InsertChange) {
//             declarationRecorder.insertLeft(change.pos, change.toAdd);
//         }
//     }
//     host.commitUpdate(declarationRecorder);
// }
//
// function addExport(host: Tree, options: ModuleOptions) {
//     const context = createAddToModuleContext(host, options, '');
//     const modulePath = options.module || '';
//
//     const exportChanges = addExportToModule(context.source,
//         modulePath,
//         context.classifiedName,
//         context.relativePath);
//
//     const exportRecorder = host.beginUpdate(modulePath);
//
//     for (const change of exportChanges) {
//         if (change instanceof InsertChange) {
//             exportRecorder.insertLeft(change.pos, change.toAdd);
//         }
//     }
//     host.commitUpdate(exportRecorder);
// }
