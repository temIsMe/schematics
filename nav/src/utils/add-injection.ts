import {DirEntry, Rule, SchematicsException, Tree} from "@angular-devkit/schematics";
import {buildRelativePath, ModuleOptions} from "../schematics-angular-utils/find-module";
import * as ts from 'typescript';
import {insertImport} from "../schematics-angular-utils/route-utils";
import {getSourceNodes} from "../schematics-angular-utils/ast-utils";
import {Change, InsertChange, NoopChange} from "../schematics-angular-utils/change";
import {constructDestinationPath} from "./find-file";
import {join, normalize, strings} from '@angular-devkit/core';


const classify = strings.classify;
const dasherize = strings.dasherize;
const camelize = strings.camelize;

interface AddInjectionContext {
    appComponentFileName: string;       // e. g. /src/app/app.component.ts
    relativeServiceFileName: string;    // e. g. ./core/side-menu/side-menu.service
    serviceName: string;                // e. g. SideMenuService
}

function findFileByName(file: string, path: string, host: Tree): string {

    let dir: DirEntry | null = host.getDir(path);

    while (dir) {
        let appComponentFileName = dir.path + '/' + file;
        if (host.exists(appComponentFileName)) {
            return appComponentFileName;
        }
        dir = dir.parent;
    }
    throw new SchematicsException(`File ${file} not found in ${path} or one of its anchestors`);
}

function createAddInjectionContext(options: ModuleOptions, host: Tree): AddInjectionContext {

    let appComponentFileName = findFileByName('app.component.ts', options.path || '/', host);
    let destinationPath = constructDestinationPath(options);
    let serviceName = classify(`${options.name}Service`);
    let serviceFileName = join(normalize(destinationPath), `${dasherize(options.name)}.service`);
    let relativeServiceFileName = buildRelativePath(appComponentFileName, serviceFileName);

    return {
        appComponentFileName,
        relativeServiceFileName,
        serviceName
    }
}

export function injectSome(options: ModuleOptions): Rule {
    console.log('-----------------     injectSome      --------------');
    return (host: Tree) => {
        let fileName = findFileByName('api.route.js', '/server/routes/', host);
        let line = 'this is my line';

        let context = {
            fileName,
            line
        };

        let text = host.read(context.fileName);
        if (!text)
            throw new SchematicsException(`File ${context.fileName} does not exist.`);

        let sourceText = text.toString('utf-8');
       // console.log("sourceText", sourceText);
        let sourceFile = ts.createSourceFile(context.fileName, sourceText, ts.ScriptTarget.Latest, true);
      //  console.log("sourceFile", sourceFile);


      

        let nodes = getSourceNodes(sourceFile);
     //   console.log("nodes",nodes.map(x => ts.SyntaxKind[parseInt(x.kind)]));
        let ctorNode = nodes.find(n => n.kind == ts.SyntaxKind.NewKeyword);
        console.log("ctorNode", ctorNode);

        let constructorChange: Change;
    

/*
        let ctorNode = nodes.find(n => n.kind == ts.SyntaxKind.Constructor);

        let constructorChange: Change;

        if (!ctorNode) {
            // No constructor found
            constructorChange = createConstructorForInjection(context, nodes, options);
        }
        else {
            constructorChange = addConstructorArgument(context, ctorNode, options);
        }

        let changes = [
            constructorChange,
            insertImport(sourceFile, context.appComponentFileName, context.serviceName, context.relativeServiceFileName)
        ];


        const declarationRecorder = host.beginUpdate(context.appComponentFileName);
        for (let change of changes) {
            if (change instanceof InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        host.commitUpdate(declarationRecorder);
*/
        return host;
    };
}


export function injectServiceIntoAppComponent(options: ModuleOptions): Rule {
    console.log('injectServiceIntoAppComponent');
    return (host: Tree) => {

        let context = createAddInjectionContext(options, host);

        let changes = buildInjectionChanges(context, host, options);

        const declarationRecorder = host.beginUpdate(context.appComponentFileName);
        for (let change of changes) {
            if (change instanceof InsertChange) {
                declarationRecorder.insertLeft(change.pos, change.toAdd);
            }
        }
        host.commitUpdate(declarationRecorder);

        return host;
    };
}

function buildInjectionChanges(context: AddInjectionContext, host: Tree, options: ModuleOptions): Change[] {

    let text = host.read(context.appComponentFileName);
    if (!text) throw new SchematicsException(`File ${options.module} does not exist.`);
    let sourceText = text.toString('utf-8');

    let sourceFile = ts.createSourceFile(context.appComponentFileName, sourceText, ts.ScriptTarget.Latest, true);

    let nodes = getSourceNodes(sourceFile);
    // console.log(nodes.map(x => x.kind));

    let ctorNode = nodes.find(n => n.kind == ts.SyntaxKind.Constructor);

    let constructorChange: Change;

    if (!ctorNode) {
        // No constructor found
        constructorChange = createConstructorForInjection(context, nodes, options);
    }
    else {
        constructorChange = addConstructorArgument(context, ctorNode, options);
    }

    return [
        constructorChange,
        insertImport(sourceFile, context.appComponentFileName, context.serviceName, context.relativeServiceFileName)
    ];

}

function addConstructorArgument(context: AddInjectionContext, ctorNode: ts.Node, options: ModuleOptions): Change {

    let siblings = ctorNode.getChildren();

    let parameterListNode = siblings.find(n => n.kind === ts.SyntaxKind.SyntaxList);

    if (!parameterListNode) {
        throw new SchematicsException(`expected constructor in ${context.appComponentFileName} to have a parameter list`);
    }

    let parameterNodes = parameterListNode.getChildren();

    let paramNode = parameterNodes.find(p => {
        let typeNode = findSuccessor(p, [ts.SyntaxKind.TypeReference, ts.SyntaxKind.Identifier]);
        if (!typeNode) return false;
        return typeNode.getText() === context.serviceName;
    });

    if (!paramNode && parameterNodes.length == 0) {
        let toAdd = `private ${camelize(context.serviceName)}: ${classify(context.serviceName)}`;
        return new InsertChange(context.appComponentFileName, parameterListNode.pos, toAdd);
    }
    else if (!paramNode && parameterNodes.length > 0) {
        let toAdd = `,
    private ${camelize(context.serviceName)}: ${classify(context.serviceName)}`;
        let lastParameter = parameterNodes[parameterNodes.length - 1];
        return new InsertChange(context.appComponentFileName, lastParameter.end, toAdd);

    }

    return new NoopChange();
}

function findSuccessor(node: ts.Node, searchPath: ts.SyntaxKind[]) {
    let children = node.getChildren();
    let next: ts.Node | undefined = undefined;

    for (let syntaxKind of searchPath) {
        next = children.find(n => n.kind == syntaxKind);
        if (!next) return null;
        children = next.getChildren();
    }
    return next;
}


function createConstructorForInjection(context: AddInjectionContext, nodes: ts.Node[], options: ModuleOptions): Change {
    let classNode = nodes.find(n => n.kind === ts.SyntaxKind.ClassKeyword);

    if (!classNode) {
        throw new SchematicsException(`expected class in ${context.appComponentFileName}`);
    }

    if (!classNode.parent) {
        throw new SchematicsException(`expected constructor in ${context.appComponentFileName} to have a parent node`);
    }

    let siblings = classNode.parent.getChildren();
    let classIndex = siblings.indexOf(classNode);

    siblings = siblings.slice(classIndex);

    let classIdentifierNode = siblings.find(n => n.kind === ts.SyntaxKind.Identifier);

    if (!classIdentifierNode) {
        throw new SchematicsException(`expected class in ${context.appComponentFileName} to have an identifier`);
    }

    if (classIdentifierNode.getText() !== 'AppComponent') {
        throw new SchematicsException(`expected first class in ${context.appComponentFileName} to have the name AppComponent`);
    }

    let curlyNodeIndex = siblings.findIndex(n => n.kind === ts.SyntaxKind.FirstPunctuation);

    siblings = siblings.slice(curlyNodeIndex);

    let listNode = siblings.find(n => n.kind === ts.SyntaxKind.SyntaxList);

    if (!listNode) {
        throw new SchematicsException(`expected first class in ${context.appComponentFileName} to have a body`);
    }

    let toAdd = `
  constructor(private ${camelize(context.serviceName)}: ${classify(context.serviceName)}) {
    // ${camelize(context.serviceName)}.show = true;
  }
`;
    return new InsertChange(context.appComponentFileName, listNode.pos + 1, toAdd);

}

function showTree(node: ts.Node, depth: number = 0): void {
    let indent = ''.padEnd(depth * 4, ' ');
    console.log(indent + ts.SyntaxKind[node.kind]);
    if (node.getChildCount() === 0) {
        console.log(indent + '    Text: ' + node.getText());
    }

    for (let child of node.getChildren()) {
        showTree(child, depth + 1);
    }
}

/*
export function addRoute(ngModulePath: string, source: ts.SourceFile, route: string): Change[] {
    const routes = getListOfRoutes(source);
    if (!routes) return [];

    if (routes.hasTrailingComma || routes.length === 0) {
        return [new InsertChange(ngModulePath, routes.end, route)];
    } else {
        return [new InsertChange(ngModulePath, routes.end, `, ${route}`)];
    }
}

function getListOfRoutes(source: ts.SourceFile): ts.NodeArray<ts.Expression> {
    const imports: any = getMatchingProperty(source, 'imports');

    if (imports.initializer.kind === ts.SyntaxKind.ArrayLiteralExpression) {
        const a = imports.initializer as ts.ArrayLiteralExpression;

        for (let e of a.elements) {
            if (e.kind === ts.SyntaxKind.CallExpression) {
                const ee = e as ts.CallExpression;
                const text = ee.expression.getText(source);
                if (
                    (text === 'RouterModule.forRoot' ||
                        text === 'RouterModule.forChild') &&
                    ee.arguments.length > 0
                ) {
                    const routes = ee.arguments[0];
                    if (routes.kind === ts.SyntaxKind.ArrayLiteralExpression) {
                        return (routes as ts.ArrayLiteralExpression).elements;
                    }
                }
            }
        }
    }
    return null;
}

function getMatchingProperty(source: ts.SourceFile,  property: string): ts.ObjectLiteralElement {
    const nodes = getDecoratorMetadata(source, 'NgModule', '@angular/core');
    let node: any = nodes[0]; // tslint:disable-line:no-any
    if (!node) return null;

    // Get all the children property assignment of object literals.
    return getMatchingObjectLiteralElement(node, source, property);
}

function getMatchingObjectLiteralElement(node: any, source: ts.SourceFile, property: string) {
    return (
        (node as ts.ObjectLiteralExpression).properties
            .filter(prop => prop.kind == ts.SyntaxKind.PropertyAssignment)
            // Filter out every fields that's not "metadataField". Also handles string literals
            // (but not expressions).
            .filter((prop: ts.PropertyAssignment) => {
                const name = prop.name;
                switch (name.kind) {
                    case ts.SyntaxKind.Identifier:
                        return (name as ts.Identifier).getText(source) === property;
                    case ts.SyntaxKind.StringLiteral:
                        return (name as ts.StringLiteral).text === property;
                }
                return false;
            })[0]
    );
}*/